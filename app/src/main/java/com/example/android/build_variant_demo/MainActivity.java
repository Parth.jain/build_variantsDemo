package com.example.android.build_variant_demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView textView = (TextView) findViewById(R.id.textview);


        if(BuildConfig.FLAVOR.equals("alpha")) {
            textView.setText("This is Alpha Build Variant");
        }
        else if(BuildConfig.FLAVOR.equals("beta")) {
            textView.setText("This is beta Build Variants");
        }
    }
}
